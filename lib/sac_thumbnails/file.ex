defmodule SacThumbnails.File do
  def site_to_filename(url) do
    url
    |> URI.parse()
    |> (fn x ->
          Regex.replace(
            ~r/([.](com.*|br.*|me.*|org.*|ong.*|a-zA-Z[\/].*)|www[.])/,
            x.host,
            ""
          )
          |> String.replace([".", "/"], "-")
          |> (fn y -> y <> String.replace(x.path, [".", "/", "_", "+"], "-") end).()
        end).()
    |> String.slice(0..50)
  end

  def get_size_of_image(data) do
    size = Map.get(data, "size", ~s(200x200))
    [h, w | _] = String.split(size, "x")
    {height, _} = data |> Map.get("height", h) |> Integer.parse()
    {width, _} = data |> Map.get("width", w) |> Integer.parse()
    {height, width}
  end

  def delete_if_blank(filename) do
    size_of_file = size_of_file(filename)

    case size_of_file == 425 do
      true ->
        File.rm!(filename)

        {:ok, json} =
          Jason.encode(%{
            data: %{
              status: false,
              message: "não foi possível gerar sua Thumbanail, tente novamente"
            }
          })

        json

      false ->
        {:ok, file} = File.read(filename)
        Base.encode64(file)
    end
  end

  def size_of_file(filename) do
    filename |> File.stat() |> (fn {_, x} -> x.size end).()
  end

  def size_of_images_equal?(filename, {height, width}) do
    %_{height: h, width: w} = filename |> Mogrify.open() |> Mogrify.verbose()
    h == height and w == width
  end

  def normalize_wildcard(ext) do
    ext
    |> String.trim(".")
    |> (fn x -> "/*." <> x end).()
  end

  def normalize_ext(ext) do
    ext
    |> String.trim(".")
    |> (fn x -> "." <> x end).()
  end

  def normalize_list_ext(list) do
    for l <- list do
      normalize_ext(l)
    end
  end

  def white_list_clear(dir, whitelist \\ [".jpg", ".png"])

  def white_list_clear(dir, whitelist) when is_binary(whitelist) do
    whitelist = [normalize_ext(whitelist)]
    remove_if_not_listed(dir, whitelist)
  end

  def white_list_clear(dir, whitelist) do
    whitelist = normalize_list_ext(whitelist)
    remove_if_not_listed(dir, whitelist)
  end

  def mem_clear(dir, ext \\ [])
  def mem_clear(dir, :all), do: Path.wildcard(dir <> "/*") |> Enum.each(&File.rm_rf!(&1))
  def mem_clear(dir, :til), do: Path.wildcard(dir <> "/*~") |> Enum.each(&File.rm_rf!(&1))

  def mem_clear(dir, ext) when is_binary(ext) do
    ext = normalize_wildcard(ext)

    Path.wildcard(dir <> ext)
    |> Enum.each(&File.rm_rf!(&1))
  end

  def mem_clear(dir, ext) do
    for e <- ext do
      mem_clear(dir, e)
    end
  end

  def white_list_clear(dir, whitelist) when is_binary(whitelist) do
    whitelist = [normalize_ext(whitelist)]
    remove_if_not_listed(dir, whitelist)
  end

  def white_list_clear(dir, whitelist) do
    whitelist = normalize_list_ext(whitelist)
    remove_if_not_listed(dir, whitelist)
  end

  def remove_if_not_listed(dir, whitelist) do
    Path.wildcard(dir <> "/*")
    |> Enum.each(fn x ->
      y = Path.extname(x)

      unless y in whitelist do
        File.rm_rf!(x)
      end
    end)
  end

  def clear_multiplies_folders(dir, ext \\ [])
  def clear_multiplies_folders([], _ext), do: nil
  def clear_multiplies_folders(dir, ext) when is_binary(dir), do: mem_clear(dir, ext)

  def clear_multiplies_folders(dir, ext) do
    for d <- dir do
      mem_clear(d, ext)
    end
  end

  def random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end
end
