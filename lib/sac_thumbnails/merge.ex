defmodule SacThumbnails.Merge do
  def run(background_image, foreground_image, output_path, opts \\ []) do
    args =
      add_gravity([], opts)
      |> add_geometry(opts)
      |> add_blend(opts)
      |> add_blur(opts)
      |> add_source(background_image)
      |> add_destination(foreground_image)
      |> add_compose(opts)
      |> add_alpha(opts)
      |> save_to(output_path)

    System.cmd("composite", args)
  end

  def add_source(args, src) do
    args ++ ["#{src}"]
  end

  def add_destination(args, dst) do
    args ++ ["#{dst}"]
  end

  def add_compose(args, opts) do
    args ++ ["-compose", Keyword.get(opts, :compose, "DstOver")]
  end

  def add_gravity(args, opts) do
    args ++ ["-gravity", Keyword.get(opts, :gravity, "center") |> String.capitalize()]
  end

  def add_geometry(args, opts) do
    args ++
      (Keyword.get(opts, :geometry, nil)
       |> case do
         nil ->
           []

         geometry ->
           ["-geometry", geometry]
       end)
  end

  def add_blend(args, opts) do
    args ++
      (Keyword.get(opts, :blend, nil)
       |> case do
         nil -> []
         blend -> ["-blend", blend]
       end)
  end

  def add_blur(args, opts) do
    args ++
      (Keyword.get(opts, :blur, nil)
       |> case do
         nil -> []
         blur -> ["-blur", blur]
       end)
  end

  def add_dissolve(args, opts) do
    args ++
      (Keyword.get(opts, :dissolve, nil)
       |> case do
         nil -> []
         dissolve -> ["-dissolve", dissolve]
       end)
  end

  def add_alpha(args, opts) do
    args ++
      (Keyword.get(opts, :alpha, nil)
       |> case do
         nil -> []
         alpha -> ["-alpha", alpha |> String.capitalize()]
       end)
  end

  def save_to(args, path) do
    args ++ ["#{path}"]
  end
end
