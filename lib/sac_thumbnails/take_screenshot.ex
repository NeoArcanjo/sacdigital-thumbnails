defmodule SacThumbnails.TakeScreenshot do
  def get(url, saveto) do
    chrome_path =
      case System.cmd("which", ~w(google-chrome)) do
        {value, 0} -> String.replace(value, "\n", "")
        {_error, 1} -> nil
      end

    params =
      ~w(--headless --disable-gpu --window-size=1280,1696 --no-sandbox --screenshot=#{saveto} 
      #{url})

    System.cmd(chrome_path, params)
  end
end
