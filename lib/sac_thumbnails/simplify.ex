defmodule SacThumbnails.Simplify do
  import Mogrify
  import SacThumbnails
  import SacThumbnails.File

  alias SacThumbnails.Merge
  alias SacThumbnails.Draw
  alias SacThumbnails.TakeScreenshot

  @video ~w(.mp4 .mpeg .3gp .gif)
  @image ~w(.png .pns .jpeg .jpg .jpe .bmp .svgz .tiff)
  @pdf ~w(.pdf)
  @docs ~w(.doc .docx .xls .xlsx .ppt .pptx .csv .txt .cdr)
  # @miscelaneous ~w(.idea .sket .dwg .dxf .rle .dib .cgm  .eps .epsf .ps .emf .gif )
  @ilustrator ~w(.ai .ait .draw .line .eps .epsf .ps .ai .ait )
  @coreldraw ~w()
  @photoshop ~w(.psd .pdd)
  @designer @ilustrator ++ @coreldraw ++ @photoshop
  @ext_default ~s(.jpg)
  @opts [width: 200, height: 200, max_height: 10_000_000, max_height: 10_000_000, circle: false]

  @validate_fields [:fill, :pointsize, :blur, :strokewidth, :font]
  @image_opts [pointsize: 80, fill: :white, strokewidth: 3, font: "Lucida-Bold-Italic"]
  @markdown_opts [pointsize: 32, fill: :black, strokewidth: 3, font: "Lucida-Bold-Italic"]

  # def create_watermark(
  #       input_path \\ "priv/images/opa.png",
  #       output_path \\ "priv/media/thumbs/aloha.png",
  #       opts \\ []
  #     ) do
  #   opts = Keyword.merge(@markdown_opts, opts)
  #   text = Keyword.get(opts, :text)
  #   height = Keyword.get(opts, :height, 185)

  #   open(input_path)
  #   |> gravity(:center)
  #   |> apply_opts(opts)
  #   |> Draw.text(0, height, text)
  #   |> save(path: output_path)
  # end

  def apply_opts(image, []) do
    image
    |> custom(:pointsize, 72)
    |> custom(:fill, :white)
    |> custom(:strokewidth, 3)
    |> custom(:font, "Lucida-Bold-Italic")
  end

  def apply_opts(image, opts) do
    opts
    |> validate_fields()
    |> Enum.reduce(image, fn {key, value}, image ->
      image |> custom(key, value)
    end)
  end

  def validate_fields(opts) do
    opts
    |> Enum.filter(fn {key, _value} -> key in @validate_fields end)
  end

  def url_valid?(url) do
    uri = URI.parse(url)
    uri.scheme != nil && uri.host =~ "."
  end

  def merge_image(foreground_image, background_image, output_path, opts \\ []) do
    Merge.run(background_image, foreground_image, output_path, opts)
  end

  def get_nome(text) when is_bitstring(text), do: text |> String.split(" ") |> get_nome

  def get_nome([nome | []]), do: nome

  def get_nome([nome | sobrenomes]) do
    sobrenome = List.last(sobrenomes)
    ~s(#{nome} #{sobrenome})
  end

  def insert_text(input_path, text, opts \\ []) do
    opts = Keyword.merge(@image_opts, opts)
    |> IO.inspect
    height = Keyword.get(opts, :height, 185)
    width = Keyword.get(opts, :width, 185)
    rotate = Keyword.get(opts, :rotate, 0)
    gravity = Keyword.get(opts, :gravity, :center)

    open(input_path)
    |> apply_opts(opts)
    |> Draw.text(width, height, rotate, gravity, text)
    |> save(path: input_path, in_place: true)
  end

  @spec output_filename(
          binary
          | maybe_improper_list(
              binary | maybe_improper_list(any, binary | []) | char,
              binary | []
            ),
          binary
          | (binary ->
               binary
               | maybe_improper_list(
                   binary | maybe_improper_list(any, binary | []) | byte,
                   binary | []
                 ))
        ) :: binary
  def output_filename(input_path, output_ext \\ "") do
    extension = input_path |> Path.extname()

    input_path
    |> Path.basename()
    |> String.replace(extension, output_ext)
  end

  @spec get_atom(any) :: any
  def get_atom(extension) when extension in @video, do: :video
  def get_atom(extension) when extension in @image, do: :image
  def get_atom(extension) when extension in @pdf, do: :pdf
  def get_atom(extension) when extension in @docs, do: :docs
  def get_atom(extension) when extension in @designer, do: :designer
  def get_atom(""), do: :link
  def get_atom(extension), do: extension

  def create_thumb(%{binary: image}, output_path, opts) when is_list(opts) do
    %Mogrify.Image{buffer: true, format: "jpeg", ext: ".jpg"}
    |> resize("200x200")
    |> verbose()
    |> custom("fill", image)
    |> create(path: output_path)
  end

  def create_thumb(url, output_path, opts) when is_list(opts) and is_binary(url) do
    unless url_valid?(url) do
      atom = url |> Path.extname() |> get_atom()
      create_thumb(atom, url, output_path, opts)
    else
      create_thumb(:link, url, output_path, opts)
    end
  end

  @spec create_thumb(
          :docs | :image | :link | :pdf | :video,
          binary
          | maybe_improper_list(
              binary | maybe_improper_list(any, binary | []) | char,
              binary | []
            ),
          binary,
          [
            {:circle, any} | {:height, any} | {:width, any} | {:max_height, any},
            ...
          ]
        ) :: :ok | {:error, atom}
  def create_thumb(atom, input_path, output_path, opts \\ @opts)

  def create_thumb(:image, input_path, output_path, opts) do
    circle = Keyword.get(opts, :circle, false)
    extension = if circle, do: ~s(.png), else: @ext_default
    output = input_path |> output_filename(extension)

    create_thumbnail(input_path, output_path <> "/" <> output, opts)
  end

  def create_thumb(:video, input_path, output_path, opts) do
    circle = Keyword.get(opts, :circle, false)
    extension = if circle, do: ~s(.png), else: @ext_default
    output = input_path |> output_filename(extension)

    create_thumbnail(input_path, output_path <> "/" <> output, opts)
  end

  def create_thumb(:designer, input_path, output_path, opts) do
    tmp = input_path |> output_filename(~s(.jpg))

    params =
      ~w(-colorspace sRGB -density 600) ++
        ["#{input_path}"] ++
        ~w(-background white -flatten -units pixelsperinch -density 224.993) ++ ["#{tmp}"]

    System.cmd("convert", params)
    create_thumb(:image, tmp, output_path, opts)
    # File.rm(tmp)
  end

  def create_thumb(:pdf, input_path, output_path, opts) do
    tmp = input_path |> output_filename(~s(.jpg))
    System.cmd("convert", [input_path <> "[0]", tmp])
    create_thumb(:image, tmp, output_path, opts)
    # File.rm(tmp)
  end

  def create_thumb(:docs, input_path, output_path, opts) do
    pdf = output_path <> output_filename(input_path, ".pdf")

    office_params =
      ~w(--headless --invisible --convert-to pdf) ++
        [
          ~S(--infilter="Text \(encoded\):UTF8,,Courier New,en-US"),
          "#{input_path}",
          "--outdir",
          "#{output_path}"
        ]

    System.cmd("libreoffice", office_params)
    create_thumb(:pdf, pdf, output_path, opts)
    File.rm(pdf)
  end

  def create_thumb(:link, input_path, output_path, opts) do
    tmp = output_path <> site_to_filename(input_path) <> @ext_default

    TakeScreenshot.get(input_path, tmp)
    create_thumb(:image, tmp, output_path, opts)
  end

  # def create_thumb(extension, input_path, output_path, opts) do
  #   circle = Keyword.get(opts, :circle, false)
  #   ext = if circle, do: ~s(.png), else: @ext_default
  #   output = input_path |> output_filename(ext)

  #   create_watermark("priv/File-icon.png", output_path <> output,
  #     gravity: :center,
  #     fill: :white,
  #     text: extension
  #   )
  # end

  def create_if_not_exist(filename, url, out_path, height, width, circle) do
    case File.exists?(filename) do
      true ->
        case size_of_images_equal?(filename, {height, width}) do
          true -> nil
          false -> create_thumb(url, out_path, height: height, width: width, circle: circle)
        end

      false ->
        create_thumb(url, out_path, height: height, width: width, circle: circle)
    end

    :ok
  end
end
