defmodule SacThumbnails.Draw do
  import Mogrify

  @spec circle(%{operations: [any]}, number, number, number, number) :: %{operations: [...]}
  def circle(image, originX, originY, perimX, perimY) do
    image
    |> custom(
      :draw,
      "circle #{
        to_string(
          :io_lib.format("~g,~g ~g,~g", [
            originX / 2,
            originY / 2,
            perimX / 2,
            perimY / 2
          ])
        )
      }"
    )
  end

  def ellipse(image, originX, originY, perimX, perimY, angleX, angleY) do
    image
    |> custom(
      :draw,
      "ellipse #{
        to_string(
          :io_lib.format("~g,~g ~g,~g ~g,~g", [
            originX / 2,
            originY / 2,
            perimX / 2,
            perimY / 2,
            angleX / 1,
            angleY / 1
          ])
        )
      }"
    )
  end

  def text(image, x, y, rotate, gravity, text) do
    image
    |> custom(:draw, "gravity #{gravity} rotate #{rotate} text #{x},#{y} '#{text}'")
  end

  def resize_image(filename, widht, height) do
    case System.cmd("mogrify", ["-resize", "#{widht}x#{height}", filename]) do
      {"", 0} -> {:ok}
      {_error, 1} -> {:error}
    end
  end
end
