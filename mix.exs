defmodule SacThumbnails.MixProject do
  use Mix.Project

  @version "0.3.4-rc2"

  def project do
    [
      app: :sac_thumbnails,
      version: @version,
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison],
      mod: {SacThumbnails.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:mogrify, "~> 0.7.3"},
      {:ffmpex, "~> 0.7.0"},
      {:httpoison, "~> 1.6"},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false}
    ]
  end

  defp description do
    "Criação de Thumbnails de imagens, vídeos, pdf e Arquivos Office"
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "CHANGELOG*", "LICENSE*"],
      maintainers: ["Rafael Arcanjo"],
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/NeoArcanjo/sacdigital-thumbnails/"}
    ]
  end
end
