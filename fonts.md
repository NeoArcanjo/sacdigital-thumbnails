Para adicionar fontes em todo o sistema, copie as novas fontes no *`/usr/share/fonts/diretório`* É uma
boa idéia criar um novo subdiretório, como *`local/`* ou similar, para ajudar a distinguir entre fontes instaladas pelo usuário e fontes padrão.

Para adicionar fontes para um usuário individual, copie as novas fontes para o diretório *`.fonts/`*  no diretório *`home`* do usuário.

Use o comando fc-cache para atualizar o cache de informações da fonte, como no exemplo a seguir:

``` bash
fc-cache <path-to-font-directory>
```

Neste comando, substitua <path-to-font-directory> pelo diretório que contém as novas fontes (/usr/share/fonts/local/ ou /home/<user>/.fonts/).