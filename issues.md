## ![warning](https://api.iconify.design/ion-md-warning.svg?height=16) Aviso !! ![warning](https://api.iconify.design/ion-md-warning.svg?height=16) ##

`ImageMagick v7 no CentOS 7 deve ser instalado da Source.`

Segui o tutorial abaixo:

[Install Imagemagick in Linux](https://www.tecmint.com/install-imagemagick-in-linux)

na Seção *`Installing ImageMagick 7 from Source Code`* e que se encontra transcrito no final deste arquivo.

Entretanto, a dependência *`lcms-devel`* não constava em meus Repos e tive de adicionar o repositório Trinity.

``` bash
# rpm -i http://mirror.ppa.trinitydesktop.org/trinity/rpm/el7/trinity-r14/RPMS/noarch/trinity-repo-14.0.7-1.el7.noarch.rpm
```

### Installing ImageMagick 7 from Source Code ###

Para instalar o ImageMagick a partir do código-fonte, você precisa de um ambiente de desenvolvimento adequado com um compilador e ferramentas de desenvolvimento relacionadas. Se você não possui os pacotes necessários em seu sistema, instale as ferramentas de desenvolvimento conforme mostrado:

``` bash
# yum groupinstall 'Development Tools'
# yum -y install bzip2-devel freetype-devel libjpeg-devel libpng-devel /libtiff-devel giflib-devel zlib-devel ghostscript-devel djvulibre-devel /libwmf-devel jasper-devel libtool-ltdl-devel libX11-devel libXext-devel /libXt-devel lcms-devel libxml2-devel librsvg2-devel OpenEXR-devel php-devel
```

Agora, baixe a versão mais recente do código fonte do ImageMagick usando o seguinte comando wget e extraia-o.

```bash
# wget https://www.imagemagick.org/download/ImageMagick.tar.gz
# tar xvzf ImageMagick.tar.gz
```

Configure e compile o código fonte do ImageMagick. Dependendo das especificações de hardware do servidor, isso pode levar algum tempo para terminar.

``` bash
# cd ImageMagick*
# ./configure
# make
# make install
```

Verifique se a compilação e instalação do ImageMagick foram bem-sucedidas.

``` bash
# magick -version
```
