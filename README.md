# Sac-Thumbnails #

Processo em Elixir para criação de Thumbnails de imagens, vídeos, pdf e Arquivos Office

## Requisitos: ##

    * ImageMagick (Para tratamento de imagens e PDFs)
    * GhostScript (Necessário para converter os PDFs)
    * FFMPEG      (Para tratamento de vídeos)
    * libreoffice (Para tratamento de arquivos office)
    * chrome instalado e com alias "chrome" adicionado ao $Path

## Instalação: ##

Add this to your `mix.exs` file, then run `mix do deps.get, deps.compile`:

``` elixir
{:sac_thumbnails, "~> 0.2.7"},
```

## Como usar: ##

Importe a biblioteca para seu modulo

``` elixir
import SacThumbnails
```

Chame a função create_thumb/3

``` elixir

```

A função espera um atom que representa o tipo de arquivo:

``` elixir
(:image | :video | :pdf | :docs | :link) # ( :docs -> Arquivos do Pacote Office)
```

e um keyword string com parametros de origem e destino, assim como opcionais de dimensinamento e forma:

``` elixir
filename,
to_path #(diretório de destino do arquivo. Ele precisa existir previamente,
:width #(largura da thumb, se for omitido será usado valor 200 por padrao),:height #(altura da thumb, se for omitido será usado valor 200 por padrao)
```

## Exemplo ##

``` elixir 
SacThumbnails.create_thumb(:image, %{filename: "/path/to/image/image.jpg", to_path: "teste/", width: 150, height: 150})
```

## Instalando ImageMagick ##
#### Centos 7 ####

* [Instalar ImageMagick](issues.md)

## Instalando FFmpeg ##
#### Centos 7 ####

[FFmpeg](https://linuxize.com/post/how-to-install-ffmpeg-on-centos-7/)

1 O repositório do Nux depende do repositório de software EPEL. Se o repositório EPEL não estiver ativado no seu sistema, ative-o digitando::

``` zsh
# yum install epel-release
```

2 Em seguida, importe a chave GPG do repositório e ative o repositório Nux instalando o pacote rpm:

``` bash
# rpm -v --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
# rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
```

3 Depois que o repositório estiver ativado, instale o FFmpeg:

``` bash
# yum install ffmpeg ffmpeg-devel
```

4 Verifique a instalação do FFmpeg executando o comando ffmpeg -version:

``` bash
# ffmpeg -version
```

#### Fedora 31/30/29 ####

Existem duas etapas para instalar o FFmpeg no Fedora.

##### *Etapa 1: Configurar o Repositório RPMfusion Yum* #####

Pacotes FFmpeg estão disponíveis no repositório RPMfusion. Adicione-o ao seu Fedora usando os comandos abaixo.

``` bash
$ sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
$ sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```


##### *Etapa 2: Instale o FFmpeg no Fedora 31/30/29* #####
Após o repositório ser adicionado, continue com a instalação do FFmpeg no Fedora.

``` bash
$ sudo dnf -y instala o ffmpeg
```

Instale bibliotecas de desenvolvimento executando o comando \:

``` bash
sudo dnf -y instala o ffmpeg-devel
```

##### *Etapa 3: verificar a versão do FFmpeg* #####
Verifique a versão do FFmpeg instalada no seu Fedora sistema usando o comando.

``` bash
$ ffmpeg -version
```

## Instalando LibreOffice 6 ##

#### Centos 7 ####

##### *Etapa 1: Remova as versões anteriores do LibreOffice* #####

``` bash
# yum remove libreoffice libreoffice-headless -y
```

##### *Etapa 2: Faça o download do pacote Linux do LibreOffice* #####

``` bash
# wget -c http://download.documentfoundation.org/libreoffice/stable/6.3.5/rpm/x86_64/LibreOffice_6.3.5_Linux_x86-64_rpm.tar.gz
```

##### *Etapa 3: Extraia o pacote do LibreOffice e o diretório de acesso* #####

``` bash
# tar -xvf LibreOffice_*
# cd LibreOffice_*
```

##### *Etapa 4: Instale / atualize os pacotes do LibreOffice RPM* #####

``` bash
# yum localinstall RPMS/*.rpm
```

##### *Etapa 5: Crie um link simbolico para LibreOffice* #####

``` bash
# ln -s /usr/bin/libreoffice6.3 /usr/bin/libreoffice
```

##### *Etapa 6: Verifique a versão* #####

``` bash
# libreoffice --version
```